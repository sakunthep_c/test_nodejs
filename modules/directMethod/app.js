"use strict";

const chalk = require("chalk");
const SerialPort = require("serialport");
var Mqtt = require("azure-iot-device-mqtt").Mqtt;
var DeviceClient = require("azure-iot-device").Client;

var connectionString =
  "HostName=bpnsgdv-ems-iothub.azure-devices.net;DeviceId=TonkDeviceDirectMethod;SharedAccessKey=hDgpH0G1fSHVPZYHzH1a2KF9f130vqPhjRH3Li5Ky2c=";
var client = DeviceClient.fromConnectionString(connectionString, Mqtt);
const port = new SerialPort(
  "/dev/ttyUSB0",
  {
    baudRate: 9600,
  },
  function (err) {
    if (err) console.log("Error: ", err.message);
  }
);

function onSetTelemetryInterval(request, response) {
  function directMethodResponse(err) {
    if (err) {
      console.error(
        chalk.red(
          "An error ocurred when sending a method response:\n" + err.toString()
        )
      );
    } else {
      console.log(
        chalk.green(
          "Response to method '" + request.methodName + "' sent successfully."
        )
      );
    }
  }
  if (request.payload == "ON") {
    port.write("ON Lighting", function (err) {
      if (err) {
        return console.log("Error on write: ", err.message);
      }
      console.log("message written");
    });
  } else {
    port.write("OFF Lighting", function (err) {
      if (err) {
        return console.log("Error on write: ", err.message);
      }
      console.log("message written");
    });
  }

  console.log(chalk.green("Direct method payload received:"));
  console.log(chalk.green(request.payload));

  response.send(200, request.payload, directMethodResponse);
}

client.onDeviceMethod("SetTelemetryInterval", onSetTelemetryInterval);

port.on("readable", function () {
  console.log("Data:", port.read().toString());
});
