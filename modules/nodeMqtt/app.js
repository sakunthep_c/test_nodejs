var mqtt = require("mqtt");

var settings = {
  clientId: "Tonk-99999",
  username: "",
  password: "",
  keepalive: 1,
  clean: false,
  reconnectPeriod: 1000 * 1,
};

var mqtt = require("mqtt");
var client = mqtt.connect("mqtt://broker.mqttdashboard.com", settings);

client.on("connect", function () {
  console.log("connected to the server. ID :" + settings.clientId);
});

client.on("message", function (topic, message) {
  console.log(message.toString());
});

client.on("error", function (error) {
  console.log("ERROR: ", error);
});

client.on("offline", function () {
  console.log("offline");
});

client.on("reconnect", function () {
  console.log("reconnect");
});

client.subscribe("tonk/1", { qos: 1 });

//start sending
var i = 0;
setInterval(function () {
  var message = i.toString();
  console.log("sending ", message);
  client.publish("tonk/1", message, { qos: 1 }, function () {
    console.log("sent ", message);
  });
  i += 1;
}, 3000);
