"use strict";

// create an empty modbus client
var ModbusRTU = require("modbus-serial");
var client = new ModbusRTU();

// open connection to a tcp line
client.connectRTUBuffered("/dev/ttyUSB1", {
  baudRate: 9600,
  parity: "none",
  dataBits: 8,
  stopBits: 1,
});
client.setID(96);
console.log("STARTTTTT");
// read the values of 10 registers starting at address 0
// on device number 1. and log the values to the console.
let i = 0;
setInterval(function () {
  console.log("INTERVAL");
  client.writeRegister(0, ++i, function (err, data) {
    console.log(data);
    client.readHoldingRegisters(0, 1, function (err, data) {
      console.log(data);
    });
  });
}, 1000);
